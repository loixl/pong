package luro.pong;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements ActionListener, KeyListener
{
	private static final char leftUpKey = 'f';
	private static final char leftDownKey = 'v';
	private static final char rightUpKey = 'h';
	private static final char rightDownKey = 'n';
	
	private Game game;
	
	private boolean leftUpPressed;
	private boolean leftDownPressed;
	private boolean rightUpPressed;
	private boolean rightDownPressed;
	
	public Controller()
	{
		game = new Game(this, this);
		game.start();
	}

	public static void main(String[] args)
	{
		new Controller();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String cmd = e.getActionCommand();
		if (cmd.equals("quit"))
		{
			System.exit(0);
		}
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		char key = e.getKeyChar();
		
		if (key == leftUpKey)
			leftUpPressed = true;
		else if (key == leftDownKey)
			leftDownPressed = true;
		else if (key == rightUpKey)
			rightUpPressed = true;
		else if (key == rightDownKey)
			rightDownPressed = true;
		
		updateMovement();
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		char key = e.getKeyChar();
		
		if (key == leftUpKey)
			leftUpPressed = false;
		else if (key == leftDownKey)
			leftDownPressed = false;
		else if (key == rightUpKey)
			rightUpPressed = false;
		else if (key == rightDownKey)
			rightDownPressed = false;
		
		updateMovement();
	}

	private void updateMovement()
	{
		if (leftUpPressed && leftDownPressed)
			game.setLeftMovement(0);
		else if (leftUpPressed)
			game.setLeftMovement(-1);
		else if (leftDownPressed)
			game.setLeftMovement(1);
		else
			game.setLeftMovement(0);

		if (rightUpPressed && rightDownPressed)
			game.setRightMovement(0);
		else if (rightUpPressed)
			game.setRightMovement(-1);
		else if (rightDownPressed)
			game.setRightMovement(1);
		else
			game.setRightMovement(0);
	}
	
	
	
	
}
