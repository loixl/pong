package luro.pong;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Game extends Thread
{
	private static final double moveSpeed = 200.0;
	private static final double sps = 40.0;
	private static final int playerHeight = 60;
	private static final int ballRadius = 5;
	
	private JFrame frame;
	private JPanel canvas;
	
	private double leftPos;
	private double rightPos;
	private int leftMov;
	private int rightMov;
	
	private double ballPosX;
	private double ballPosY;
	private double ballHdg;
	private double ballSpd;
	
	@SuppressWarnings("serial")
	public Game(ActionListener al, KeyListener kl)
	{
		frame = new JFrame("Pong");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setLocationRelativeTo(null);
		
		frame.addKeyListener(kl);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		JMenuItem quitItem = new JMenuItem("Quit");
		quitItem.setActionCommand("quit");
		quitItem.addActionListener(al);
		fileMenu.add(quitItem);
		
		canvas = new JPanel()
		{
			@Override
			public void paintComponent(Graphics g)
			{
				g.clearRect(0, 0, getWidth(), getHeight());
				
				g.setColor(Color.black);
				g.fillRect(0, (int)leftPos, 20, playerHeight);
				g.fillRect(getWidth()-20, (int)rightPos, 20, playerHeight);

				int ballx = (int)ballPosX - ballRadius;
				int bally = (int)ballPosY - ballRadius;
				g.setColor(Color.red);
				g.fillOval(ballx, bally, ballRadius*2, ballRadius*2);
			}
		};
		
		frame.add(canvas);
		frame.setVisible(true);
		
		leftPos = 0.0;
		rightPos = 0.0;
		leftMov = 0;
		rightMov = 0;
		
		ballPosX = (double)canvas.getWidth()/2.0;
		ballPosY = (double)canvas.getHeight()/2.0;
		ballHdg = Math.PI/2.0;
		ballSpd = 50.0;
	}
	
	public void run()
	{
		long t;
		long interval = (long)(1000.0/sps);
		
		while(true)
		{
			t = System.currentTimeMillis();
			
			if ((leftMov!=0) || (rightMov!=0))
			{
				leftPos = leftPos + ((double)leftMov)*(moveSpeed/sps);
				rightPos = rightPos + ((double)rightMov)*(moveSpeed/sps);
				
				double max = (double)canvas.getHeight()-(double)playerHeight;
				if (leftPos > max)
					leftPos = max;
				else if (leftPos < 0.0)
					leftPos = 0.0;
				if (rightPos > max)
					rightPos = max;
				else if (rightPos < 0.0)
					rightPos = 0.0;
			}
			
			canvas.repaint();
			
			try
			{
				sleep(interval-(System.currentTimeMillis()-t));
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void setLeftMovement(int movement)
	{
		leftMov = movement;
	}
	public void setRightMovement(int movement)
	{
		rightMov = movement;
	}
}
